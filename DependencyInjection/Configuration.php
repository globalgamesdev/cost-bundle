<?php

namespace CostBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $treeBuilder->root('cost')
            ->children()
                ->arrayNode('source')
                    ->children()
                        ->arrayNode('jetgamer')
                            ->children()
                                ->scalarNode('guzzle')
                                    ->info('Guzzle client for jetgamer api requests')
                                    ->isRequired()
                                    ->cannotBeEmpty()
                                    ->defaultValue("@skins.cost.guzzle.client")
                                ->end()
                                ->scalarNode('token')
                                    ->info('Token for Post request on jetgamer api endpoint')
                                    ->isRequired()
                                    ->cannotBeEmpty()
                                ->end()
                                ->scalarNode('host')
                                    ->info('Path to jetgamer api endpoint')
                                    ->isRequired()
                                    ->cannotBeEmpty()
                                ->end()
                                ->integerNode('priority')
                                    ->info('Priority of jetgamer api source')
                                    ->defaultValue(100)
                                ->end()
                                ->scalarNode('hash_hmac')
                                    ->isRequired()
                                    ->cannotBeEmpty()
                                    ->defaultValue('sha256')
                                ->end()
                                ->scalarNode('public_hash')
                                    ->info('Public hash on jetgamer, for hash_hmac algorithm')
                                    ->isRequired()
                                    ->cannotBeEmpty()
                                ->end()
                                ->scalarNode('private_hash')
                                    ->info('Private hash on jetgamer, for hash_hmac algorithm')
                                    ->isRequired()
                                    ->cannotBeEmpty()
                                ->end()
                            ->end()
                        ->end()
                        ->arrayNode('redis')
                            ->children()
                                ->scalarNode('redis_client')
                                    ->info('Redis client')
                                    ->isRequired()
                                    ->cannotBeEmpty()
                                    ->defaultValue("@snc_redis.cost")
                                ->end()
                                ->integerNode('priority')
                                    ->info('Priority of redis source')
                                    ->defaultValue(200)
                                ->end()
                                ->integerNode('ttl')
                                    ->info('Seconds for redis storage')
                                    ->defaultValue(600)
                                ->end()
                                ->scalarNode('hash_algorithm')
                                    ->info('Redis key hash algorithm')
                                    ->isRequired()
                                    ->cannotBeEmpty()
                                    ->defaultValue('md5')
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('strategy')
                    ->children()
                        ->scalarNode('not_zero_price_strategy')
                            ->info('Priority of not zero price strategy')
                            ->defaultValue(100)
                        ->end()
                    ->end()
                ->end()
            ->end();

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $treeBuilder;
    }
}
