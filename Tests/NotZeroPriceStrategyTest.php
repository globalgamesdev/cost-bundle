<?php

namespace CostBundle\Tests;

use Doctrine\Common\Collections\ArrayCollection;
use CostBundle\Service\NotZeroPriceStrategy;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class NotZeroPriceStrategyTest
 * @package CostBundle\Tests
 */
class NotZeroPriceStrategyTest extends KernelTestCase
{
    /**
     * @var int
     */
    private $priority;

    /**
     * @var ArrayCollection
     */
    private $skins;

    /**
     * @var ArrayCollection
     */
    private $result;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        self::bootKernel();
        $this->priority = self::$kernel->getContainer()->getParameter('skins_cost_strategy_not_zero_priority');
        $this->skins = new ArrayCollection(['fake_1'=>0,'fake_2'=>100]);
        $this->result = new ArrayCollection(['fake_2'=>100]);

    }

    /**
     * Test priority of class
     */
    public function testGetPriority()
    {
        $strategy = new NotZeroPriceStrategy(
            $this->priority
        );
        $this->assertEquals(
            $this->priority,
            $strategy->getPriority()
        );
    }

    /**
     * test sort algorithm by strategy
     */
    public function testSort()
    {
        $strategy = new NotZeroPriceStrategy(
            $this->priority
        );
        $this->assertEquals(
            $this->result,
            $strategy->sort($this->skins)
        );
    }
}
