<?php

namespace CostBundle\Tests;

use Doctrine\Common\Collections\ArrayCollection;
use GuzzleHttp\Client;
use Psr\Http\Message\MessageInterface;
use CostBundle\Service\JetgamerSource;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class JetgamerSourceTest
 * @package CostBundle\Tests
 */
class JetgamerSourceTest extends KernelTestCase
{
    /**
     * @var int
     */
    private $priority;

    /**
     * @var Client
     */
    private $guzzle;

    /**
     * @var string
     */
    private $token;

    /**
     * @var string
     */
    private $host;

    /**
     * @var ArrayCollection
     */
    private $skins;
    /**
     * @var array
     */
    private $skinsResult;

    /**
     * @var string
     */
    private $hash_hmac;

    /**
     * @var string
     */
    private $public;

    /**
     * @var string
     */
    private $private;

    /**
     * @var MessageInterface
     */
    private $response;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        self::bootKernel();

        $this->token = self::$kernel->getContainer()->getParameter('skins_cost_api_token');
        $this->priority = self::$kernel->getContainer()->getParameter('skins_cost_api_priority');
        $this->host = self::$kernel->getContainer()->getParameter('skins_cost_api_host');
        $this->hash_hmac = self::$kernel->getContainer()->getParameter('skins_cost_api_hash_hmac');
        $this->public = self::$kernel->getContainer()->getParameter('skins_cost_api_public_hash');
        $this->private = self::$kernel->getContainer()->getParameter('skins_cost_api_private_hash');
        $this->skins = new ArrayCollection(['fake_skin_1' => 0, 'fake_skin_2' => 0]);
        $this->skinsResult = [
            'items' => [
                ['name' => 'fake_skin_1', 'price' => 1],
                ['name' => 'fake_skin_2', 'price' => 2],
            ],
        ];

        $this->guzzle = $this->createMock(Client::class);
        $this->response = $this->createMock(MessageInterface::class);

        $this->responseJson = ['avatarfull' => 'text'];
    }

    /**
     * Test priority of class
     */
    public function testGetPriority()
    {
        $api = new JetgamerSource(
            $this->guzzle,
            $this->token,
            $this->host,
            $this->priority,
            $this->hash_hmac,
            $this->public,
            $this->private
        );
        $this->assertEquals(
            $this->priority,
            $api->getPriority()
        );
    }

    /**
     * Test for parsing
     */
    public function testDoParse()
    {
        $this->response->expects($this->any())
            ->method('getBody')
            ->will($this->returnValue(json_encode($this->skinsResult)));

        $this->guzzle->expects($this->any())
            ->method('request')
            ->will($this->returnValue($this->response));

        $api = new JetgamerSource(
            $this->guzzle,
            $this->token,
            $this->host,
            $this->priority,
            $this->hash_hmac,
            $this->public,
            $this->private
        );

        $this->assertEquals(
            new ArrayCollection(['fake_skin_1' => 100, 'fake_skin_2' => 200]),
            $api->doEstimateCost($this->skins)
        );
    }
}
