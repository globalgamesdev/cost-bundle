<?php

namespace CostBundle\Tests;

use Doctrine\Common\Collections\ArrayCollection;
use Predis\Client;
use CostBundle\Service\RedisSource;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class RedisSourceTest
 * @package CostBundle\Tests
 */
class RedisSourceTest extends KernelTestCase
{
    /**
     * @var int
     */
    private $priority;

    /**
     * @var Client;
     */
    private $redis;

    /**
     * @var ArrayCollection
     */
    private $skins;
    /**
     * @var ArrayCollection
     */
    private $skinsResult;

    /**
     * @var string
     */
    private $hashAlgorithm;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        self::bootKernel();

        $this->priority = self::$kernel->getContainer()->getParameter('skins_cost_redis_priority');
        $this->hashAlgorithm = self::$kernel->getContainer()->getParameter('skins_cost_redis_storage_hash_algorithm');
        $this->redis = $this
            ->getMockBuilder(Client::class)
            ->setMethods(['get'])
            ->getMock();
        $this->skins = new ArrayCollection(['fake_skin_1' => 0, 'fake_skin_2' => 0]);
        $this->skinsResult = new ArrayCollection(['fake_skin_1' => 100, 'fake_skin_2' => 100]);

    }

    /**
     * Test priority of class
     */
    public function testGetPriority()
    {
        $api = new RedisSource(
            $this->redis,
            $this->priority,
            $this->hashAlgorithm
        );
        $this->assertEquals(
            $this->priority,
            $api->getPriority()
        );
    }

    /**
     * Test for parsing
     */
    public function testDoParse()
    {
        $this->redis->expects($this->any())
            ->method('get')
            ->will($this->returnValue(100));

        $api = new RedisSource(
            $this->redis,
            $this->priority,
            $this->hashAlgorithm
        );

        $this->assertEquals(
            $this->skinsResult,
            $api->doEstimateCost($this->skins)
        );
    }
}
