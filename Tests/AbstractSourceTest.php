<?php

namespace CostBundle\Tests;

use Doctrine\Common\Collections\ArrayCollection;
use Predis\Client;
use CostBundle\Service\AbstractSource;
use CostBundle\Service\RedisSource;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class AbstractSourceTest
 * @package CostBundle\Tests
 */
class AbstractSourceTest extends KernelTestCase
{
    /**
     * @var ArrayCollection
     */
    private $need;

    /**
     * @var ArrayCollection
     */
    private $already;

    /**
     * @var AbstractSource;
     */
    private $parser;
    /**
     * @var Client
     */
    private $redis;

    /**
     * @var string
     */
    private $hashAlgorithm;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        self::bootKernel();
        $this->need = new ArrayCollection(['fake_1' => false, 'fake_2' => false]);
        $this->already = new ArrayCollection(['fake_1' => 100, 'fake_2' => 100]);
        $this->parser = $this->createMock(RedisSource::class);
        $this->hashAlgorithm = self::$kernel->getContainer()->getParameter('skins_cost_redis_storage_hash_algorithm');

        $this->redis = $this
            ->getMockBuilder(Client::class)
            ->setMethods(['get'])
            ->getMock();
    }

    /**
     * Test parse
     */
    public function testEstimateCost()
    {
        $this->redis->expects($this->any())
            ->method('get')
            ->will($this->returnValue(100));

        $parser = new RedisSource(
            $this->redis,
            0,
            $this->hashAlgorithm
        );

        $this->assertEquals(
            $this->already,
            $parser->estimateCost($this->need, new ArrayCollection())
        );
    }

    /**
     * Test doParse
     */
    public function testDoEstimateCost()
    {
        $this->redis->expects($this->any())
            ->method('get')
            ->will($this->returnValue(100));

        $parser = new RedisSource(
            $this->redis,
            0,
            $this->hashAlgorithm
        );

        $this->assertEquals(
            $this->already,
            $parser->doEstimateCost($this->need)
        );
    }
}
