<?php

namespace CostBundle\Tests;

use Doctrine\Common\Collections\ArrayCollection;
use Predis\Client;
use CostBundle\Service\AbstractSource;
use CostBundle\Service\CostStrategy;
use CostBundle\Service\Sources;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class SourcesTest
 * @package CostBundle\Tests
 */
class SourcesTest extends KernelTestCase
{
    /**
     * @var ArrayCollection
     */
    private $skins = [];

    /**
     * @var ArrayCollection
     */
    private $sortedSkins = [];

    /**
     * @var ArrayCollection
     */
    private $finishSkins = [];

    /**
     * @var Client
     */
    private $redis;

    /**
     * @var AbstractSource
     */
    private $parser;

    /**
     * @var CostStrategy
     */
    private $strategy;

    /**
     * @var int
     */
    private $ttl;
    /**
     * @var string
     */
    private $hashAlgorithm;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        self::bootKernel();

        $this->redis = $this
            ->getMockBuilder(Client::class)
            ->setMethods(['get', 'setEx', 'exists'])
            ->getMock();
        $this->parser = $this->createMock(AbstractSource::class);
        $this->strategy = $this->createMock(CostStrategy::class);
        $this->ttl = self::$kernel->getContainer()->getParameter('skins_cost_redis_storage_ttl');
        $this->hashAlgorithm = self::$kernel->getContainer()->getParameter('skins_cost_redis_storage_hash_algorithm');

        $this->sortedSkins = new ArrayCollection(['fake_1' => 100, 'fake_2' => 200]);
        $this->skins = new ArrayCollection([[
            "appid" => 730,
            "contextid" => "2",
            "market_name" => "fake_1",
            "assetid" => "10086598997",
            "classid" => "2076480239",
            "instanceid" => "188530139",
            "amount" => 1,
            "pos" => 1,
            "id" => "10086598997"
        ], [
            "appid" => 730,
            "contextid" => "2",
            "market_name" => "fake_2",
            "assetid" => "10086598997",
            "classid" => "2076480239",
            "instanceid" => "188530139",
            "amount" => 1,
            "pos" => 1,
            "id" => "10086598997"
        ]]);
        $this->finishSkins = new ArrayCollection([[
            "appid" => 730,
            "contextid" => "2",
            "market_name" => "fake_1",
            "assetid" => "10086598997",
            "classid" => "2076480239",
            "instanceid" => "188530139",
            "amount" => 1,
            "pos" => 1,
            "id" => "10086598997",
            "price" => 100
        ], [
            "appid" => 730,
            "contextid" => "2",
            "market_name" => "fake_2",
            "assetid" => "10086598997",
            "classid" => "2076480239",
            "instanceid" => "188530139",
            "amount" => 1,
            "pos" => 1,
            "id" => "10086598997",
            "price" => 200
        ]]);
    }

    /**
     * Test get cost method
     */
    public function testGetCost()
    {
        $this->parser->expects($this->any())
            ->method('estimateCost')
            ->will($this->returnValue($this->sortedSkins));

        $this->strategy->expects($this->any())
            ->method('sort')
            ->will($this->returnValue($this->sortedSkins));

        $chain = new Sources(
            [$this->parser],
            $this->redis,
            $this->ttl,
            $this->hashAlgorithm,
            [$this->strategy]
        );
        $this->assertEquals(
            $this->finishSkins->toArray(),
            $chain->getCost($this->skins->toArray())
        );
    }
}
