<?php

namespace CostBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Exception\RuntimeException;
use Predis\Client;

/**
 * Orchestration by estimate skins cost and save them in redis storage
 *
 * Class Sources
 * @package CostBundle\Service
 */
class Sources
{
    /**
     * @var ArrayCollection
     */
    private $skins = [];

    /**
     * @var mixed|null
     */
    protected $first = null;

    /**
     * @var Client
     */
    private $redis;

    /**
     * @var array
     */
    private $strategies;

    /**
     * @var int
     */
    private $ttl;
    /**
     * @var string
     */
    private $hashAlgorithm;

    /**
     * Sources constructor.
     *
     * @param array $parsers
     * @param Client $redis
     * @param int $ttl
     * @param string $hashAlgorithm
     * @param array $strategies
     */
    public function __construct(
        array $parsers,
        Client $redis,
        int $ttl,
        string $hashAlgorithm,
        array $strategies
    ) {
        $this->ttl = $ttl;
        $this->hashAlgorithm = $hashAlgorithm;
        $this->redis = $redis;
        $this->strategies = $strategies;
        $current = null;
        $this->skins = new ArrayCollection();

        usort($parsers, [$this, "sortPriority"]);

        foreach ($parsers as $parser) {
            if (is_null($this->first)) {
                $this->first = $parser;
            }

            if (!is_null($current)) {
                $current->setNextSource($parser);
            }
            $current = $parser;
        }
    }

    /**
     * Create Skins object with unique skins and zero price
     *
     * @param array $names
     *
     * @return ArrayCollection
     */
    private function init(array $names): ArrayCollection
    {
        $names = array_unique($names);

        foreach ($names as $name) {
            $this->skins->set($name, false);
        }

        return $this->skins;
    }

    /**
     * Get cost of skins
     *
     * @param array $skins
     *
     * @return array
     */
    public function getCost(array $skins)
    {
        $names = [];

        foreach ($skins as $item) {
            $names[] = $item['market_name'];
        }

        $this->skins = $this->init($names);

        if ($this->first) {
            $res = $this->first->estimateCost($this->skins, new ArrayCollection());

            foreach ($res->toArray() as $name => $price) {
                //save to redis
                $key = hash($this->hashAlgorithm, $name);
                if (!$this->redis->exists($key)) {
                    $this->redis->setEx($key, $this->ttl, $price);
                }
            }
            $namesWithPrices = $this->sortSkins($res)->toArray();
            return $this->mergePrices($namesWithPrices, $skins);
        }
        throw new RuntimeException("Empty chain parsers!");
    }

    /**
     * Merging prices to finish skins array
     *
     * @param $namesWithPrices
     * @param $skins
     *
     * @return array
     */
    private function mergePrices($namesWithPrices, $skins)
    {
        $skinsWithPrices = [];

        foreach ($skins as $item) {
            foreach ($namesWithPrices as $name => $price) {
                if ($item['market_name'] == $name) {
                    $item['price'] = $price;
                    $skinsWithPrices[] = $item;
                }
            }
        }

        return $skinsWithPrices;
    }

    /**
     * Calculate and sort items by strategies
     *
     * @param ArrayCollection $items
     *
     * @return ArrayCollection
     */
    private function sortSkins(ArrayCollection $items)
    {
        usort($this->strategies, [$this, "sortPriority"]);

        if (empty($this->strategies)) {
            throw new RuntimeException("Empty cost strategy!");
        }

        foreach ($this->strategies as $strategy) {
            if (!$strategy instanceof CostStrategy) {
                throw new RuntimeException("Strategy is not type ofCosyStrategy!");
            }
            $items = $strategy->sort($items);
        }

        return $items;
    }

    /**
     * Sort objects (sources|strategies) by priority
     *
     * @param $a
     * @param $b
     *
     * @return int
     */
    private function sortPriority($a, $b)
    {
        return strcmp($b->getPriority(), $a->getPriority());
    }
}
