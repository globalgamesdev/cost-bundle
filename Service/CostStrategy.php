<?php

namespace CostBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Get rules for estimate cost for skins
 *
 * Interface CostStrategy
 * @package CostBundle\Service
 */
interface CostStrategy
{
    /**
     * Recalculate skins by some strategy
     *
     * @param ArrayCollection $items
     *
     * @return ArrayCollection
     */
    public function sort(ArrayCollection $items): ArrayCollection;
}
