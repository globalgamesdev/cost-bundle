<?php

namespace CostBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Abstract class for each cost source
 *
 * Class AbstractSource
 * @package CostBundle\Service
 */
abstract class AbstractSource implements CostPriority
{
    /**
     * @var AbstractSource
     */
    protected $nextSource;

    /**
     * Get skins with zero cost
     *
     * @param ArrayCollection $skins
     *
     * @return ArrayCollection|bool
     */
    protected function getEmptySkins(ArrayCollection $skins)
    {
        foreach ($skins->toArray() as $name => $price) {
            if (false !== $price) {
                $skins->remove($name);
            }
        }
        if (!$skins->isEmpty()) {
            return $skins;
        }

        return false;
    }

    /**
     * Set next source
     *
     * @param AbstractSource $nextSource
     */
    public function setNextSource(AbstractSource $nextSource)
    {
        $this->nextSource = $nextSource;
    }

    /**
     * Loop of sources and go throw each source, while exist zero price skins and exist next source
     *
     * @param ArrayCollection $needSkins - empty array which need parse
     *
     * @param ArrayCollection $alreadySkins
     *
     * @return ArrayCollection
     */
    public function estimateCost(ArrayCollection $needSkins, ArrayCollection $alreadySkins)
    {
        if ($needSkins = $this->getEmptySkins($needSkins)) {
            foreach ($this->doEstimateCost($needSkins)->toArray() as $name => $price) {
                $alreadySkins->set($name, $price);
                $needSkins->remove($name);
            }
        }

        if (null !== $this->nextSource && !$needSkins->isEmpty()) {
            return $this->nextSource->estimateCost($needSkins, $alreadySkins);
        } else {
            return $alreadySkins;
        }
    }

    /**
     * Estimate cost of skins by some source
     *
     * @param ArrayCollection $skins
     *
     * @return ArrayCollection
     */
    abstract public function doEstimateCost(ArrayCollection $skins): ArrayCollection;
}
