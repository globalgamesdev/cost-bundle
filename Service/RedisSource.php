<?php

namespace CostBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Predis\Client;

/**
 * Get skins price from Redis
 *
 * Class RedisSource
 * @package CostBundle\Service
 */
class RedisSource extends AbstractSource
{
    /**
     * @var int
     */
    private $priority;

    /**
     * @var Client;
     */
    private $redis;

    /**
     * @var ArrayCollection
     */
    private $skins;

    /**
     * @var string
     */
    private $hashAlgorithm;

    /**
     * RedisSource constructor.
     *
     * @param Client $redis
     * @param Int $priority
     * @param string $hashAlgorithm
     */
    public function __construct(Client $redis, int $priority, string $hashAlgorithm)
    {
        $this->redis = $redis;
        $this->priority = $priority;
        $this->hashAlgorithm = $hashAlgorithm;
        $this->skins = new ArrayCollection();
    }

    /**
     * @inheritdoc
     */
    public function doEstimateCost(ArrayCollection $skins): ArrayCollection
    {
        foreach ($skins->toArray() as $name => $price) {
            $key = hash($this->hashAlgorithm, $name);

            $price = $this->redis->get($key);
            if (is_numeric($price)) {
                $this->skins->set($name, $price);
            }
        }
        return $this->skins;
    }

    /**
     * @inheritdoc
     */
    public function getPriority(): int
    {
        return $this->priority;
    }
}
