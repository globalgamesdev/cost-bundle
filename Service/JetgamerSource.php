<?php

namespace CostBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use GuzzleHttp\Client;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Get skins price from Api
 *
 * Class JetgamerSource
 * @package CostBundle\Service
 */
class JetgamerSource extends AbstractSource
{
    /**
     * @var int
     */
    private $priority;

    /**
     * @var Client
     */
    private $guzzle;

    /**
     * @var string
     */
    private $token;

    /**
     * @var string
     */
    private $host;

    /**
     * @var ArrayCollection
     */
    private $skins;

    /**
     * @var string
     */
    private $hash_hmac;

    /**
     * @var string
     */
    private $public;

    /**
     * @var string
     */
    private $private;

    /**
     * JetgamerSource constructor.
     *
     * @param Client $guzzle
     * @param string $token
     * @param string $host
     * @param Int $priority
     * @param string $hash_hmac
     * @param string $public
     * @param string $private
     *
     */
    public function __construct(
        Client $guzzle,
        string $token,
        string $host,
        int $priority,
        string $hash_hmac,
        string $public,
        string $private
    ) {
        $this->guzzle = $guzzle;
        $this->token = $token;
        $this->host = $host;
        $this->priority = $priority;
        $this->hash_hmac = $hash_hmac;
        $this->public = $public;
        $this->private = $private;

        $this->skins = new ArrayCollection();
    }

    /**
     * @inheritdoc
     */
    public function doEstimateCost(ArrayCollection $skins): ArrayCollection
    {
        $skins = $skins->toArray();

        $content = json_encode(array_keys($skins));
        $hash = hash_hmac($this->hash_hmac, $content, $this->private);
        $response = $this->guzzle->request(
            'POST',
            $this->host,
            [
                'timeout' => 20,
                'headers' => [
                    'X-Public' => $this->public,
                    'X-Hash' => $hash,
                ],
                'form_params' =>
                    [
                        'token' => $this->token,
                        'items' => $content,
                    ],
            ]
        );

        $result = json_decode($response->getBody(), true);
        if (null === $result['items']) {
            throw new NotFoundHttpException('Not found items');
        }
        foreach ($result['items'] as $item) {
            $this->skins->set($item['name'], round($item['price'] * 100, 0));
        }

        return $this->skins;
    }

    /**
     * @inheritdoc
     */
    public function getPriority(): int
    {
        return $this->priority;
    }
}
