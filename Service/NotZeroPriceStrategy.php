<?php

namespace CostBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Remove all skins where price less than or equal to Zero
 *
 * Class NotZeroPriceStrategy
 * @package CostBundle\Service\EstimateStrategy
 */
final class NotZeroPriceStrategy implements CostStrategy, CostPriority
{
    /**
     * @var int
     */
    private $priority;

    /**
     * NotZeroPriceStrategy constructor.
     *
     * @param int $priority
     */
    public function __construct(int $priority)
    {
        $this->priority = $priority;
    }

    /**
     * @var ArrayCollection
     */
    private $skins;

    /**
     * @param ArrayCollection $items
     *
     * @return ArrayCollection
     */
    public function sort(ArrayCollection $items): ArrayCollection
    {
        $this->skins = new ArrayCollection();
        foreach ($items->toArray() as $name => $price) {
            if ($price > 0) {
                $this->skins->set($name, $price);
            }
        }

        return $this->skins;
    }

    /**
     * @inheritdoc
     */
    public function getPriority(): int
    {
        return $this->priority;
    }
}
