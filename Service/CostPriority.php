<?php

namespace CostBundle\Service;

/**
 * Get priority for implementation class
 *
 * Interface CostPriority
 * @package CostBundle\Service
 */
interface CostPriority
{

    /**
     * Return priority of class
     *
     * @return int
     */
    public function getPriority(): int;
}
